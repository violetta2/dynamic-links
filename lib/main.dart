import 'dart:async';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MaterialApp(
    title: 'Dynamic Links Example',
    routes: <String, WidgetBuilder>{
      '/': (BuildContext context) => _MainScreen(),
      '/test': (BuildContext context) => _DynamicLinkScreen(),
    },
  ));
}

class _MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainScreenState();
}

class _MainScreenState extends State<_MainScreen> {
  String _linkMessage;
  bool _isCreatingLink = false;
  String _testString =
      "To test: long press link and then copy and click from a browser ";
  String _json =
      '{"contract_name": "34ef3e63-73ad-4f9b-b2c9-862ade2525ae", "sign_insurance": "edsigtdmnayJ5pf9PL3YZs3RJ8ywsP2tuTXiziroVh9JUpsP8jhLRMcBztPmUAD8NdQLAWbha1VgMgRExNcznUHBMFA9HgKrKx2", "sign_proxy": "edsigu2vSrR7TmhsWty4sdpb2mavm56ruxcYM2SaWiodjKGFR6wxZipdEsqHF4UQ6wR2SxSwYrECBq19pjPYtWX96zj7VcZbDXh", "timestamp_ins": " 1606408260", "timestamp_proxy": " 1606408260"}';

  @override
  void initState() {
    super.initState();
    initDynamicLinks();
  }

  void initDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;
      final Map json = {'payload': deepLink.queryParameters['payload']};
      if (deepLink != null) {
        Navigator.pushNamed(context, deepLink.path, arguments: json);
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;
    final Map json = {'payload': deepLink.queryParameters['payload']};
    if (deepLink != null) {
      Navigator.pushNamed(context, deepLink.path, arguments: json);
    }
  }

  Future<void> _createDynamicLink(bool short) async {
    setState(() {
      _isCreatingLink = true;
    });

    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://sampleappedynamiclinks.page.link',
      link: Uri.parse('https://sampleappedynamiclinks.page.link/test' +
          "?payload=" +
          _json),
      androidParameters: AndroidParameters(
        fallbackUrl: Uri.parse('https://www.moneytrack.io/sante/assures'),
        packageName: 'com.example.dynamic_links',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        fallbackUrl: Uri.parse('https://www.moneytrack.io/sante/assures'),
        bundleId: 'com.moneytrack.flutterdynamiclinks',
        minimumVersion: '0',
      ),
    );

    Uri url;
    if (short) {
      final ShortDynamicLink shortLink = await parameters.buildShortLink();
      url = shortLink.shortUrl;
    } else {
      url = await parameters.buildUrl();
    }

    setState(() {
      _linkMessage = url.toString();
      _isCreatingLink = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Dynamic Links Example'),
        ),
        body: Builder(builder: (BuildContext context) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ButtonBar(
                  alignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: !_isCreatingLink
                          ? () => _createDynamicLink(false)
                          : null,
                      child: const Text('Get Long Link'),
                    ),
                    RaisedButton(
                      onPressed: !_isCreatingLink
                          ? () => _createDynamicLink(true)
                          : null,
                      child: const Text('Get Short Link'),
                    ),
                  ],
                ),
                InkWell(
                  child: Text(
                    _linkMessage ?? '',
                    style: const TextStyle(color: Colors.blue),
                  ),
                  onTap: () async {
                    if (_linkMessage != null) {
                      await launch(_linkMessage);
                    }
                  },
                  onLongPress: () {
                    Clipboard.setData(ClipboardData(text: _linkMessage));
                    Scaffold.of(context).showSnackBar(
                      const SnackBar(content: Text('Copied Link!')),
                    );
                  },
                ),
                Text(_linkMessage == null ? '' : _testString)
              ],
            ),
          );
        }),
      ),
    );
  }
}

class _DynamicLinkScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map args = ModalRoute.of(context).settings.arguments as Map;
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Test DeepLink'),
        ),
        body: Center(
          child: Text(args['payload'].toString()),
        ),
      ),
    );
  }
}
